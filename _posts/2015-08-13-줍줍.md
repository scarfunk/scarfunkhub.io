---
layout: post
title: 150813
comments: true
---


# 오늘의 줍줍

{% highlight javascript %}
{% endhighlight %}

### https://guides.github.com/
* 깃헙 가이드
* <https://guides.github.com/>

### es6 맛보기

* <https://gist.github.com/marocchino/841e2ff62f59f420f9d9>
* <http://ohgyun.com/580>


### http://slides.com/

* reveal.js 로 만든 프레젠테이션.

### 원시타입에 관하여.
* JS <http://bestalign.github.io/2015/06/29/JavaScript-Data-Type/>
* java 는 모두 call by value 다. 파라미터로 넘어갈때
* 원시타입은 string까지(얘는 조금 특이함)
* 원시타입은 값을 복사합니다 :)
* ref타입은 어떨까?

{% highlight java %}

Hi hi1 = new Hi();
System.out.println(hi1.hey); //this is hi
Hi hi2 = new Hi();
hi2 = hi1; // ref 복사.
System.out.println(hi2.hey); //this is hi
hi2.hey = "what?"; // 값변경
System.out.println(hi2.hey); //what?
System.out.println(hi1.hey); //what? > hi2의 변경이 h1에게 미침.

{% endhighlight %}

### 클로져 호이스팅
* 클로져 : 참조변수에 계속 접근이 가능하다. <http://mobicon.tistory.com/48>
* 호이스팅 : Javascript에서는 함수내 어디에서 변수를 선언하든 모든 함수의 최상단에서 변수가 선언된다
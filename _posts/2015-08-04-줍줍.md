---
layout: post
title: 150804
comments: true
---


# 오늘의 줍줍

### java code convention


* <http://kwangshin.pe.kr/blog/java-code-conventions-%EC%9E%90%EB%B0%94-%EC%BD%94%EB%94%A9-%EA%B7%9C%EC%B9%99/#5.1.4.%20줄 끝(End-Of-Line) 주석>
* 자바 코딩 가이드? 랄까.


### new relic

* 모니터링 툴
* tomcat 예제 <https://docs.newrelic.com/docs/agents/java-agent/getting-started/new-relic-java#installation>
* 간단히 로직을 설명하자면
	* 뉴렐릭에 가입
	* apm 에서 java - spring 해서 소스를 다운로드(token도 기입되있음)
	* 서버 톰캣 root 폴더에 소스를 unzip 하고 jar 실행
	* 2분후쯤? new relic 싸이트에 로그인 하면 모니터링이 된다.
	 
* 국산 모니터링 <http://whatap.io>

### 맥 디플로이 배포용 톰캣 설치하기.

* <http://wonsama.tistory.com/410>
* 커맨드라인에서 다루는 연습을 하자. 서버에서 놀거니깐..

### spring boot war 추출.
* <http://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#howto-create-a-deployable-war-file>
* pom.xml 에 수정 및 추가.

{% highlight xml %}
	    <packaging>war</packaging>
    
            <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-tomcat</artifactId>
            <scope>provided</scope>
        </dependency>

{% endhighlight %}
* 스프링 부트 코드에 수정.
{% highlight java %}

public class SimpleQnaApplication extends SpringBootServletInitializer{

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(SimpleQnaApplication.class);
    }
{% endhighlight %}

* mvn 클린 - 패키지 를 하면 target 에 war가 나온다
* 톰캣 webapp에 cp 하면 배포 완료
* 뉴렐릭에 들어가 보면 된다.

### cli http client

* <https://github.com/jkbrzt/httpie>
* curl 을 이쁘게...


### intelliJ auto import

* <http://stackoverflow.com/questions/16615038/what-is-the-shortcut-to-auto-import-all-in-android-studio>

